import React from "react";
import { View, Text, StyleSheet, TouchableNativeFeedback } from "react-native";

const GoalItem = props => {
  return (
    <TouchableNativeFeedback onPress={props.onDelete.bind(this, props.index)}>
      <View style={styles.listItem}>
        <Text>
          {parseInt(props.index) + 1}. {props.title}
        </Text>
      </View>
    </TouchableNativeFeedback>
  );
};

const styles = StyleSheet.create({
  listItem: {
    marginHorizontal: 5,
    borderRadius: 6,
    padding: 20,
    marginVertical: 5,
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3
  }
});

export default GoalItem;
