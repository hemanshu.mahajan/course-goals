import React, { useState } from "react";
import { View, TextInput, StyleSheet, Button, Modal } from "react-native";

const GoalInput = props => {
  const [entredGoal, setEntredGoal] = useState("");

  const goalInputHandler = entredText => {
    setEntredGoal(entredText);
  };

  const addGoalHandler = () => {
    props.onAddGoal(entredGoal);
    setEntredGoal("");
  };

  return (
    <Modal visible={props.visible} animationType="slide" transparent>
      <View style={styles.subContainer}>
        <TextInput
          placeholder="Course Goal"
          style={styles.input}
          onChangeText={goalInputHandler}
          value={entredGoal}
        />
        <View style={styles.buttons}>
          <Button title="CANCEL" color="red" onPress={props.onCancel} />
          <Button title="ADD" onPress={addGoalHandler} />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  subContainer: {
    borderWidth: 2,
    borderColor: "#ccc",
    height: "80%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    height: "90%",
    marginTop: "30%",
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30
  },
  input: {
    borderBottomColor: "black",
    borderWidth: 1,
    padding: 10,
    width: 200,
    marginBottom: 10
  },
  buttons: {
    flexDirection: "row",
    justifyContent: "space-around",
    width: "60%"
  }
});

export default GoalInput;
