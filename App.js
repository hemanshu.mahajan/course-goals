import React, { useState } from "react";
import { StyleSheet, View, FlatList, Button } from "react-native";

import GoalItem from "./src/components/GoalItems";
import GoalInput from "./src/components/GoalInput";

export default function App() {
  const [courseGoals, setCourseGoals] = useState([]);
  const [isAddMode, setIsAddMode] = useState(false);

  const addGoalHandler = goalTitle => {
    setCourseGoals(currentGoal => [...currentGoal, goalTitle]);
    setIsAddMode(false);
  };

  const removeGoalItem = goalId => {
    // console.log(goalId);
    setCourseGoals(currentGoal => {
      return currentGoal.filter(
        goal => courseGoals.indexOf(goal).toString() !== goalId
      );
    });
  };

  const cancelGoalAddAdditionalHandler = () => {
    setIsAddMode(false);
  };
  return (
    <View style={styles.container}>
      <Button title="ADD NEW GOAL" onPress={() => setIsAddMode(true)} />
      <GoalInput
        onAddGoal={addGoalHandler}
        visible={isAddMode}
        onCancel={cancelGoalAddAdditionalHandler}
      />
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={courseGoals}
        renderItem={itemData => (
          <GoalItem
            title={itemData.item}
            index={itemData.index.toString()}
            onDelete={removeGoalItem}
          />
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 50,
    paddingHorizontal: 20
  }
});
